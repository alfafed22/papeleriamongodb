package vista;

import modelo.Producto;
import io.IO;

import java.util.List;

import controlador.ControladorProducto;

/**
 * Clase que representa el menú de opciones para interactuar con la base de
 * datos MongoDB.
 */
public class MenuMongoDB {
	private ControladorProducto controladorProducto;

	/**
	 * Constructor de la clase MenuMongoDB.
	 */
	public MenuMongoDB() {
		controladorProducto = new ControladorProducto();
	}

	/**
	 * Método principal que inicia el menú y permite al usuario interactuar con la
	 * base de datos MongoDB.
	 */
	public static void main(String[] args) {
		MenuMongoDB menu = new MenuMongoDB();
		menu.mostrarMenu();
	}

	/**
	 * Método que muestra el menú de opciones y gestiona la interacción del usuario
	 * con la base de datos.
	 */
	public void mostrarMenu() {
		boolean salir = false;

		while (!salir) {
			IO.println("1. Agregar producto");
            IO.println("2. Modificar producto");
            IO.println("3. Eliminar producto");
            IO.println("4. Consultar todos los productos");
            IO.println("5. Buscar productos por campo");
            IO.println("6. Salir");
            int opcion = IO.readInt("Seleccione una opción: ");

			switch (opcion) {
			case 1:
				agregarProducto();
				break;
			case 2:
				modificarProducto();
				break;
			case 3:
				eliminarProducto();
				break;
			case 4:
				consultarProductos();
				break;
			case 5:
				buscarProductosPorCampo();
				break;
			case 6:
				salir = true;
				IO.println("Desconectando de la base de datos...");
				break;
			default:
				IO.println("Opción inválida. Intente de nuevo.");
			}
		}

	}

	/**
	 * Método que permite al usuario agregar un nuevo producto a la base de datos.
	 */
	private void agregarProducto() {
		controladorProducto.agregarProducto();
	}

	/**
	 * Método que permite al usuario modificar un producto existente en la base de
	 * datos.
	 */
	private void modificarProducto() {
		consultarProductos();
		String nombreProducto = IO.readString("Ingrese el nombre del producto que desea modificar: ");

		// Obtener la lista de productos con el nombre dado
		List<Producto> productos = controladorProducto.buscarProductosPorNombre(nombreProducto);

		// Mostrar la lista de productos encontrados
		if (productos.isEmpty()) {
			IO.println("No se encontraron productos con el nombre '" + nombreProducto + "'.");
			return;
		}

		IO.println("Productos encontrados con el nombre '" + nombreProducto + "':");
		for (int i = 0; i < productos.size(); i++) {
			Producto producto = productos.get(i);
			IO.println((i + 1) + ". " + producto);
		}

		// Solicitar al usuario que elija el número del producto a modificar
		int opcion = IO.readInt("Seleccione el número del producto que desea modificar: ");

		// Verificar si la opción seleccionada es válida
		if (opcion < 1 || opcion > productos.size()) {
			IO.println("Opción inválida. Intente de nuevo.");
			return;
		}

		// Obtener el producto seleccionado
		Producto productoSeleccionado = productos.get(opcion - 1);

		// Solicitar al usuario que elija el campo a modificar
		IO.println("Seleccione el campo que desea modificar:");
		IO.println("1. Nombre");
		IO.println("2. Cantidad");
		IO.println("3. Precio");
		IO.println("4. Proveedor");
		IO.println("5. Campos personalizados");
		opcion = IO.readInt("Seleccione una opción: ");

		switch (opcion) {
		case 1:
			controladorProducto.modificarNombre(productoSeleccionado);
			break;
		case 2:
			controladorProducto.modificarCantidad(productoSeleccionado);
			break;
		case 3:
			controladorProducto.modificarPrecio(productoSeleccionado);
			break;
		case 4:
			controladorProducto.modificarProveedor(productoSeleccionado);
			break;
		case 5:
			controladorProducto.modificarCamposPersonalizados(productoSeleccionado);
			break;
		default:
			IO.println("Opción inválida. Intente de nuevo.");
		}
	}

	/**
	 * Método que permite al usuario eliminar un producto existente en la base de
	 * datos.
	 */
	private void eliminarProducto() {
		controladorProducto.eliminarProducto();
	}

	/**
	 * Método que permite al usuario consultar todos los productos almacenados en la
	 * base de datos.
	 */
	private void consultarProductos() {
		List<Producto> productos = controladorProducto.obtenerTodosLosProductos();
		if (productos.isEmpty()) {
			IO.println("No se encontraron productos.");
		} else {
			for (Producto producto : productos) {
				IO.println(producto.toString());
			}
		}
	}
	/**
	 * Método que permite al usuario buscar productos en la base de datos por un campo específico.
	 * Este método maneja diferentes tipos de valores para el campo de búsqueda, como int, double y String.
	 */
	private void buscarProductosPorCampo() {
	    consultarProductos();
	    String nombreCampo = IO.readString("Ingrese el nombre del campo: ");
	    String valorCampoStr = IO.readString("Ingrese el valor del campo: ");

	    // Verificar si el valor del campo es numérico (int o double)
	    if (isNumeric(valorCampoStr)) {
	        // Si el valor es numérico, intentar convertirlo a int
	        try {
	            int valorCampoInt = Integer.parseInt(valorCampoStr);
	            List<Producto> productosEncontrados = controladorProducto.buscarProductosPorCampo(nombreCampo, valorCampoInt);
	            mostrarResultadosBusqueda(nombreCampo, valorCampoStr, productosEncontrados);
	        } catch (NumberFormatException e) {
	            // Si no se puede convertir a int, intentar convertir a double
	            try {
	                double valorCampoDouble = Double.parseDouble(valorCampoStr);
	                List<Producto> productosEncontrados = controladorProducto.buscarProductosPorCampo(nombreCampo, valorCampoDouble);
	                mostrarResultadosBusqueda(nombreCampo, valorCampoStr, productosEncontrados);
	            } catch (NumberFormatException ex) {
	                // Si no se puede convertir a double, buscar como String
	                buscarComoString(nombreCampo, valorCampoStr);
	            }
	        }
	    } else {
	        // Si el valor del campo no es numérico, buscar como String
	        buscarComoString(nombreCampo, valorCampoStr);
	    }
	}
	/**
	 * Método auxiliar que verifica si una cadena es numérica (int o double).
	 * @param str La cadena a verificar.
	 * @return true si la cadena es numérica, false de lo contrario.
	 */
	private boolean isNumeric(String str) {
	    // Verificar si la cadena es numérica (int o double)
	    return str.matches("-?\\d+(\\.\\d+)?");
	}
	/**
	 * Método auxiliar que realiza la búsqueda de productos por un campo de tipo String.
	 * @param nombreCampo El nombre del campo.
	 * @param valorCampoStr El valor del campo como String.
	 */
	private void buscarComoString(String nombreCampo, String valorCampoStr) {
	    List<Producto> productosEncontrados = controladorProducto.buscarProductosPorCampo(nombreCampo, valorCampoStr);
	    if (productosEncontrados.isEmpty()) {
	        IO.println("No se encontraron productos con el campo '" + nombreCampo + "' igual a '" + valorCampoStr + "'.");
	    } else {
	        IO.println("Productos encontrados:");
	        for (Producto producto : productosEncontrados) {
	            IO.println(producto.toString());
	        }
	    }
	}
	/**
	 * Método auxiliar que muestra los resultados de la búsqueda de productos.
	 * @param nombreCampo El nombre del campo.
	 * @param valorCampoStr El valor del campo como String.
	 * @param productosEncontrados La lista de productos encontrados.
	 */
	private void mostrarResultadosBusqueda(String nombreCampo, String valorCampoStr, List<Producto> productosEncontrados) {
	    if (productosEncontrados.isEmpty()) {
	        IO.println("No se encontraron productos con el campo '" + nombreCampo + "' igual a '" + valorCampoStr + "'.");
	    } else {
	        IO.println("Productos encontrados:");
	        for (Producto producto : productosEncontrados) {
	            IO.println(producto.toString());
	        }
	    }
	}

}

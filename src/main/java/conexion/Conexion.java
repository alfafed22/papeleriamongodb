package conexion;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

/**
 * Clase que proporciona una conexión a una base de datos MongoDB.
 */
public class Conexion {
    /** URL de conexión a la base de datos MongoDB. */
    private static final String CONNECTION_STRING = "mongodb+srv://alfafed22:x1dq0LazpHLcqQ43@cluster0.kombe58.mongodb.net/";

    /** Nombre de la base de datos a la que se conectará. */
    private static final String DATABASE_NAME = "papeleria";

    /** Cliente MongoDB para conectarse al servidor. */
    private static MongoClient mongoClient;

    /** Base de datos a la que se conectará. */
    private static MongoDatabase database;

    /**
     * Devuelve una conexión a la base de datos MongoDB.
     * 
     * @return Una instancia de MongoDatabase que representa la conexión a la base de datos.
     */
    public static MongoDatabase getConnection() {
        // Si el cliente MongoDB no está inicializado, crearlo
        if (mongoClient == null) {
            mongoClient = MongoClients.create(CONNECTION_STRING);
        }

        // Si la base de datos no está inicializada, obtenerla del cliente MongoDB
        if (database == null) {
            database = mongoClient.getDatabase(DATABASE_NAME);
        }

        // Devolver la base de datos
        return database;
    }
}

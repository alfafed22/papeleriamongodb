package io;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;
import java.util.UUID;

public class IO {
	private static Scanner sc = new Scanner(System.in);

	/**
	 * Constructor
	 */
	IO() {
		sc.useDelimiter("\n");
	}

	/**
	 * Muestra un objeto
	 * 
	 * @param o objeto
	 */
	static public void print(Object o) {
		System.out.print(o);
	}

	/**
	 * Muestra un objeto y salta de línea
	 * 
	 * @param o objeto
	 */
	static public void println(Object o) {
		System.out.println(o);
	}

	/**
	 * Lee un valor de tipo byte
	 * 
	 * @return
	 */
	static public byte readByte(String mensaje) {
		while (true) {
			try {
				println(mensaje);
				return Byte.parseByte(sc.nextLine());
			} catch (Exception e) {
				println("ERROR: No es de tipo byte.");
			}
		}
	}

	/**
	 * Lee un valor de tipo short
	 * 
	 * @return
	 */
	static public short readShort(String mensaje) {
		while (true) {
			try {
				println(mensaje);
				return Short.parseShort(sc.nextLine());
			} catch (Exception e) {
				println("ERROR: No es de tipo short.");
			}
		}
	}

	/**
	 * Lee un valor de tipo int
	 * 
	 * @return
	 */
	static public int readInt(String mensaje) {
		while (true) {
			try {
				println(mensaje);
				return Integer.parseInt(sc.nextLine());
			} catch (Exception e) {
				println("ERROR: No es de tipo int.");
			}
		}
	}

	/**
	 * Lee un valor de tipo long
	 * 
	 * @return
	 */
	static public long readLong(String mensaje) {
		while (true) {
			try {
				println(mensaje);
				return Long.parseLong(sc.nextLine());
			} catch (Exception e) {
				println("ERROR: No es de tipo long.");
			}
		}
	}

	/**
	 * Lee un valor de tipo float
	 * 
	 * @return
	 */
	static public float readFloat(String mensaje) {
		while (true) {
			try {
				println(mensaje);
				return Float.parseFloat(sc.nextLine());
			} catch (Exception e) {
				println("ERROR: No es de tipo float.");
			}
		}
	}

	/**
	 * Lee un valor de tipo double
	 * 
	 * @return
	 */
	static public double readDouble(String mensaje) {
		while (true) {
			try {
				println(mensaje);
				return Double.parseDouble(sc.nextLine());
			} catch (Exception e) {
				println("ERROR: No es de tipo double.");
			}
		}
	}

	/**
	 * Lee un valor de tipo boolean
	 * 
	 * @return
	 */
	static public boolean readBoolean(String mensaje) {
		while (true) {
			String s = sc.nextLine();
			if (s.equals("true"))
				return true;
			if (s.equals("false"))
				return false;
			println("ERROR: No es de tipo boolean (true o false).");
		}
	}

	/**
	 * Lee un valor de tipo char
	 * 
	 * @return
	 */
	static public char readChar(String mensaje) {
		while (true) {
			String s = sc.nextLine();
			if (s.length() == 1) {
				return s.toCharArray()[0];
			}
			println("ERROR: No es de tipo char.");
		}
	}

	/**
	 * Lee un valor de tipo LocalDate
	 * 
	 * @return
	 */
	static public LocalDate readLocalDate(String mensaje) {
		while (true) {
			try {
				println(mensaje);
				return LocalDate.parse(sc.nextLine());
			} catch (DateTimeParseException e) {
				println("ERROR: No es de tipo LocalDate.");
			}
		}
	}

	/**
	 * Lee un valor de tipo UUID
	 * 
	 * @return
	 */
	static public UUID readUUID(String mensaje) {
		while (true) {
			try {
				println(mensaje);
				return UUID.fromString(sc.nextLine());
			} catch (IllegalArgumentException e) {
				println("ERROR: No es de tipo UUID.");
			}
		}
	}

	/**
	 * Lee un valor de tipo String
	 * 
	 * @return
	 */
	static public String readString(String mensaje) {
		println(mensaje);
		return sc.nextLine();
	}
}

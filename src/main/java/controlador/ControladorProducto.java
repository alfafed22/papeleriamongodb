package controlador;

import conexion.Conexion;
import io.IO;
import modelo.Producto;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Clase que gestiona las operaciones relacionadas con los productos en la base
 * de datos MongoDB.
 */

public class ControladorProducto {
	/** Colección en la base de datos que almacena los productos. */
	private MongoCollection<Document> collection;
	/**
     * Constructor que inicializa el controlador y establece la conexión con la base de datos.
     */
	public ControladorProducto() {
		collection = Conexion.getConnection().getCollection("inventario");
	}
	 /**
     * Agrega un nuevo producto a la base de datos.
     */
	public void agregarProducto() {
		// Solicitar al usuario que ingrese los datos del producto
		String nombre = "";
		while (nombre.isEmpty()) {
			nombre = IO.readString("Ingrese el nombre del producto: ");
			if (nombre.isEmpty()) {
				IO.println("El nombre del producto no puede estar vacío. Por favor, inténtelo de nuevo.");
			}
		}

		int cantidad = -1;
		while (cantidad < 0) {
			cantidad = IO.readInt("Ingrese la cantidad del producto: ");
			if (cantidad < 0) {
				IO.println("La cantidad del producto no puede ser negativa. Por favor, inténtelo de nuevo.");
			}
		}

		double precio = -1;
		while (precio < 0) {
			precio = IO.readDouble("Ingrese el precio del producto: ");
			if (precio < 0) {
				IO.println("El precio del producto no puede ser negativo. Por favor, inténtelo de nuevo.");
			}
		}

		String proveedor = "";
		while (proveedor.isEmpty()) {
			proveedor = IO.readString("Ingrese el proveedor del producto: ");
			if (proveedor.isEmpty()) {
				IO.println("El proveedor del producto no puede estar vacío. Por favor, inténtelo de nuevo.");
			}
		}

		// Crear el producto sin ID
		Producto producto = new Producto(null, nombre, cantidad, precio, proveedor);

		// Crear el documento del producto
		Document documentoProducto = new Document("nombre", nombre).append("cantidad", cantidad)
				.append("precio", precio).append("proveedor", proveedor);

		// Preguntar al usuario si desea agregar campos personalizados
		String agregarCamposPersonalizados = IO.readString("¿Desea agregar campos personalizados? (si/no): ")
				.toLowerCase();
		if (agregarCamposPersonalizados.equals("si") || agregarCamposPersonalizados.equals("sí")) {
			boolean agregarMasCampos = true;
			while (agregarMasCampos) {
				String nombreCampo = "";
				while (nombreCampo.isEmpty()) {
					nombreCampo = IO.readString("Ingrese el nombre del campo personalizado: ");
					if (nombreCampo.isEmpty()) {
						IO.println(
								"El nombre del campo personalizado no puede estar vacío. Por favor, inténtelo de nuevo.");
					}
				}

				String valorCampo = IO.readString("Ingrese el valor del campo personalizado: ");
				documentoProducto.append(nombreCampo, valorCampo);

				String respuesta = IO.readString("¿Desea agregar otro campo personalizado? (si/no): ").toLowerCase();
				agregarMasCampos = respuesta.equals("si") || respuesta.equals("sí");
			}
		}

		// Verificar si el producto ya existe en la base de datos
		if (productoYaExiste(producto)) {
			IO.println("El producto ya existe en la base de datos.");
			return;
		}

		// Insertar el documento del producto en la colección
		collection.insertOne(documentoProducto);

		IO.println("Producto agregado correctamente.");
	}
	 /**
     * Elimina un producto de la base de datos.
     */
	public void eliminarProducto() {
		// Mostrar todos los productos de la base de datos
		Map<Integer, Producto> productosMap = new HashMap<>();
		Map<Integer, String> idMap = new HashMap<>();
		int index = 1;
		for (Document doc : collection.find()) {
			Producto producto = documentoAProducto(doc);
			productosMap.put(index, producto);
			idMap.put(index, doc.getObjectId("_id").toString());
			IO.println(index + ". " + producto.toString());
			index++;
		}

		if (productosMap.isEmpty()) {
			IO.println("No hay productos en la base de datos para eliminar.");
			return;
		}

		// Pedir al usuario que ingrese el nombre del producto que desea eliminar
		String nombreProductoEliminar = IO.readString("Ingrese el nombre del producto que desea eliminar: ");

		// Buscar productos con el nombre dado
		List<Integer> indicesProductosEncontrados = new ArrayList<>();
		for (Map.Entry<Integer, Producto> entry : productosMap.entrySet()) {
			if (entry.getValue().getNombre().equalsIgnoreCase(nombreProductoEliminar)) {
				indicesProductosEncontrados.add(entry.getKey());
			}
		}

		if (indicesProductosEncontrados.isEmpty()) {
			IO.println("No se encontraron productos con el nombre '" + nombreProductoEliminar + "'.");
			return;
		}

		// Mostrar los productos encontrados y sus índices
		IO.println("Productos encontrados:");
		for (Integer indice : indicesProductosEncontrados) {
			Producto producto = productosMap.get(indice);
			IO.println(indice + ". " + producto.toString());
		}

		// Pedir al usuario que elija un índice
		int indiceSeleccionado = IO.readInt("Seleccione el número del producto que desea eliminar: ");

		// Verificar si el índice seleccionado es válido
		if (indicesProductosEncontrados.contains(indiceSeleccionado)) {
			// Obtener el ID asociado al índice seleccionado
			String idProductoSeleccionado = idMap.get(indiceSeleccionado);

			// Eliminar el producto seleccionado
			Document resultado = collection.find(new Document("_id", new ObjectId(idProductoSeleccionado))).first();
			if (resultado != null) {
				collection.deleteOne(resultado);
				IO.println("Producto '" + nombreProductoEliminar + "' eliminado correctamente.");
			} else {
				IO.println("No se encontró ningún producto con el nombre '" + nombreProductoEliminar + "'.");
			}
		} else {
			IO.println("Índice inválido. Por favor, seleccione un número válido.");
		}
	}
	/**
     * Busca productos en la base de datos por su nombre.
     * @param nombre El nombre del producto a buscar.
     * @return Una lista de productos encontrados con el nombre dado.
     */
	public List<Producto> buscarProductosPorNombre(String nombre) {
		List<Producto> productosEncontrados = new ArrayList<>();
		Document filtro = new Document("nombre", nombre);
		for (Document doc : collection.find(filtro)) {
			Producto producto = documentoAProducto(doc);
			productosEncontrados.add(producto);
		}
		return productosEncontrados;
	}
	/**
	 * Busca productos en la base de datos por un campo específico con un valor de tipo String.
	 * @param nombreCampo El nombre del campo por el cual buscar.
	 * @param valorCampo El valor del campo a buscar (tipo String).
	 * @return Una lista de productos encontrados que coinciden con el campo y valor dados.
	 */
	public List<Producto> buscarProductosPorCampo(String nombreCampo, String valorCampo) {
	    List<Producto> productosEncontrados = new ArrayList<>();
	    Document filtro = new Document(nombreCampo, valorCampo);
	    for (Document doc : collection.find(filtro)) {
	        Producto producto = documentoAProducto(doc);
	        productosEncontrados.add(producto);
	    }
	    return productosEncontrados;
	}
	/**
	 * Busca productos en la base de datos por un campo específico con un valor de tipo int.
	 * @param nombreCampo El nombre del campo por el cual buscar.
	 * @param valorCampo El valor del campo a buscar (tipo int).
	 * @return Una lista de productos encontrados que coinciden con el campo y valor dados.
	 */
	public List<Producto> buscarProductosPorCampo(String nombreCampo, int valorCampo) {
	    List<Producto> productosEncontrados = new ArrayList<>();
	    Document filtro = new Document(nombreCampo, valorCampo);
	    for (Document doc : collection.find(filtro)) {
	        Producto producto = documentoAProducto(doc);
	        productosEncontrados.add(producto);
	    }
	    return productosEncontrados;
	}
	/**
	 * Busca productos en la base de datos por un campo específico con un valor de tipo double.
	 * @param nombreCampo El nombre del campo por el cual buscar.
	 * @param valorCampo El valor del campo a buscar (tipo double).
	 * @return Una lista de productos encontrados que coinciden con el campo y valor dados.
	 */
	public List<Producto> buscarProductosPorCampo(String nombreCampo, double valorCampo) {
	    List<Producto> productosEncontrados = new ArrayList<>();
	    Document filtro = new Document(nombreCampo, valorCampo);
	    for (Document doc : collection.find(filtro)) {
	        Producto producto = documentoAProducto(doc);
	        productosEncontrados.add(producto);
	    }
	    return productosEncontrados;
	}
	/**
	 * Verifica si un producto ya existe en la base de datos MongoDB.
	 * 
	 * @param producto El producto que se desea verificar si existe.
	 * @return true si el producto ya existe en la base de datos, false en caso contrario.
	 */
	private boolean productoYaExiste(Producto producto) {
		// Convertir los valores de los campos a minúsculas para evitar duplicados
		String nombre = producto.getNombre().toLowerCase();
		String proveedor = producto.getProveedor().toLowerCase();

		// Verificar si el producto ya existe en la base de datos
		Document filtro = new Document("nombre", nombre).append("precio", producto.getPrecio()).append("proveedor",
				proveedor);
		Document productoExistente = collection.find(filtro).first();
		if (productoExistente != null) {
			// Comparar campos personalizados
			Map<String, Object> camposProductoExistente = new HashMap<>();
			for (Map.Entry<String, Object> entry : productoExistente.entrySet()) {
				String clave = entry.getKey();
				if (!clave.equals("_id") && !clave.equals("nombre") && !clave.equals("cantidad")
						&& !clave.equals("precio") && !clave.equals("proveedor")) {
					camposProductoExistente.put(clave, entry.getValue());
				}
			}
			Map<String, Object> camposProductoNuevo = producto.getCamposPersonalizados();

			return camposProductoExistente.equals(camposProductoNuevo);
		}
		return false;
	}
	/**
     * Convierte un documento de MongoDB en un objeto Producto.
     * @param documento El documento MongoDB a convertir.
     * @return El objeto Producto correspondiente al documento.
     */
	// Método auxiliar para convertir un Document a Producto
	private Producto documentoAProducto(Document documento) {
		ObjectId id = documento.getObjectId("_id"); // Obtener el ID del documento
		String nombre = documento.getString("nombre");
		int cantidad = documento.getInteger("cantidad");
		double precio = documento.getDouble("precio");
		String proveedor = documento.getString("proveedor");

		// Crear el producto con el ID obtenido del documento
		Producto producto = new Producto(id, nombre, cantidad, precio, proveedor);

		// Obtener los campos personalizados y agregarlos al producto
		for (Map.Entry<String, Object> entry : documento.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			// Excluir los campos reservados y agregar los demás directamente al producto
			if (!key.equals("_id") && !key.equals("nombre") && !key.equals("cantidad") && !key.equals("precio")
					&& !key.equals("proveedor")) {
				producto.agregarCampoPersonalizado(key, value.toString());
			}
		}

		return producto;
	}
	/**
     * Convierte un objeto Producto en un documento de MongoDB.
     * @param producto El objeto Producto a convertir.
     * @return El documento MongoDB correspondiente al producto.
     */
	public Document productoADocumento(Producto producto) {
		Document documento = new Document();
		documento.append("nombre", producto.getNombre()).append("cantidad", producto.getCantidad())
				.append("precio", producto.getPrecio()).append("proveedor", producto.getProveedor());

		// Agregar campos personalizados como atributos independientes
		Map<String, Object> camposPersonalizados = producto.getCamposPersonalizados();
		for (Map.Entry<String, Object> entry : camposPersonalizados.entrySet()) {
			documento.append(entry.getKey(), entry.getValue());
		}

		return documento;
	}
	 /**
     * Obtiene todos los productos almacenados en la base de datos.
     * @return Una lista de todos los productos almacenados.
     */
	public List<Producto> obtenerTodosLosProductos() {
		List<Producto> productos = new ArrayList<>();
		MongoCursor<Document> cursor = collection.find().iterator();
		try {
			while (cursor.hasNext()) {
				Document doc = cursor.next();
				Producto producto = documentoAProducto(doc);
				productos.add(producto);
			}
		} finally {
			cursor.close();
		}
		return productos;
	}
	/**
     * Modifica el nombre de un producto.
     * @param producto El producto al que se le modificará el nombre.
     */
	public void modificarNombre(Producto producto) {
		String nuevoNombre = IO.readString("Ingrese el nuevo nombre del producto: ");
		producto.setNombre(nuevoNombre);
		// Guardar los cambios en la base de datos
		guardarCambios(producto);
	}
	/**
     * Modifica la cantidad de un producto.
     * @param producto El producto al que se le modificará la cantidad.
     */
	public void modificarCantidad(Producto producto) {
		int nuevaCantidad = IO.readInt("Ingrese la nueva cantidad del producto: ");
		producto.setCantidad(nuevaCantidad);
		// Guardar los cambios en la base de datos
		guardarCambios(producto);
	}
	/**
     * Modifica el precio de un producto.
     * @param producto El producto al que se le modificará el precio.
     */
	public void modificarPrecio(Producto producto) {
		double nuevoPrecio = IO.readDouble("Ingrese el nuevo precio del producto: ");
		producto.setPrecio(nuevoPrecio);
		// Guardar los cambios en la base de datos
		guardarCambios(producto);
	}
	/**
     * Modifica el proveedor de un producto.
     * @param producto El producto al que se le modificará el proveedor.
     */
	public void modificarProveedor(Producto producto) {
		String nuevoProveedor = IO.readString("Ingrese el nuevo proveedor del producto: ");
		producto.setProveedor(nuevoProveedor);
		// Guardar los cambios en la base de datos
		guardarCambios(producto);
	}
	/**
     * Modifica los campos personalizados de un producto.
     * @param producto El producto al que se le modificarán los campos personalizados.
     */
	public void modificarCamposPersonalizados(Producto producto) {
		// Mostrar los campos personalizados actuales
		mostrarCamposPersonalizados(producto);

		// Pedir al usuario que ingrese el nombre del campo que desea modificar
		String nombreCampoModificar = IO.readString("Ingrese el nombre del campo que desea modificar: ");

		// Verificar si el campo personalizado existe en el producto
		if (producto.getCamposPersonalizados().containsKey(nombreCampoModificar)) {
			// Pedir al usuario que ingrese el nuevo valor para el campo
			String nuevoValorCampo = IO
					.readString("Ingrese el nuevo valor para el campo '" + nombreCampoModificar + "': ");

			// Modificar el valor del campo personalizado en el producto
			producto.getCamposPersonalizados().put(nombreCampoModificar, nuevoValorCampo);

			// Guardar los cambios en la base de datos
			guardarCambios(producto);
			IO.println("Producto modificado correctamente.");
		} else {
			IO.println("El campo '" + nombreCampoModificar + "' no existe en el producto.");
		}
	}
	/**
     * Muestra los campos personalizados de un producto.
     * @param producto El producto del que se mostrarán los campos personalizados.
     */
	private void mostrarCamposPersonalizados(Producto producto) {
		IO.println("Campos personalizados actuales:");
		Map<String, Object> camposPersonalizados = producto.getCamposPersonalizados();
		for (Map.Entry<String, Object> entry : camposPersonalizados.entrySet()) {
			IO.println(entry.getKey() + ": " + entry.getValue());
		}
	}
	/**
     * Guarda los cambios realizados en un producto en la base de datos.
     * @param producto El producto al que se le han realizado cambios.
     */
	private void guardarCambios(Producto producto) {
		// Crear un documento con los nuevos valores del producto
		Document nuevoDocumento = new Document("nombre", producto.getNombre())
				.append("cantidad", producto.getCantidad()).append("precio", producto.getPrecio())
				.append("proveedor", producto.getProveedor());

		// Agregar campos personalizados al documento
		for (Map.Entry<String, Object> entry : producto.getCamposPersonalizados().entrySet()) {
			nuevoDocumento.append(entry.getKey(), entry.getValue());
		}

		// Actualizar el documento del producto en la base de datos
		collection.updateOne(eq("_id", producto.getId()), new Document("$set", nuevoDocumento));
	}
	/**
     * Genera un filtro para la igualdad del campo fieldName con el valor de id.
     * @param fieldName El nombre del campo.
     * @param id El valor del campo.
     * @return Un filtro para la igualdad del campo fieldName con el valor de id.
     */
	private Bson eq(String fieldName, ObjectId id) {
		// Devolver un filtro para la igualdad del campo fieldName con el valor de id
		return Filters.eq(fieldName, id);
	}

}

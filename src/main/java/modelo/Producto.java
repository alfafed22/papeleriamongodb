package modelo;

import java.util.HashMap;
import java.util.Map;

import org.bson.Document;
import org.bson.types.ObjectId;

/**
 * Clase que representa un producto en el sistema.
 */
public class Producto {
    private ObjectId id;
    private String nombre;
    private int cantidad;
    private double precio;
    private String proveedor;
    private Map<String, Object> camposPersonalizados;

    /**
     * Constructor de la clase Producto.
     */
    public Producto(ObjectId id, String nombre, int cantidad, double precio, String proveedor) {
        this.id = id;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
        this.proveedor = proveedor;
        this.camposPersonalizados = new HashMap<>();
    }

    /**
     * Método para obtener el ID del producto.
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Método para establecer el ID del producto.
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Método para obtener el nombre del producto.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Método para establecer el nombre del producto.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Método para obtener la cantidad del producto.
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * Método para establecer la cantidad del producto.
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * Método para obtener el precio del producto.
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * Método para establecer el precio del producto.
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * Método para obtener el proveedor del producto.
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * Método para establecer el proveedor del producto.
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * Método para obtener los campos personalizados del producto.
     */
    public Map<String, Object> getCamposPersonalizados() {
        return camposPersonalizados;
    }

    /**
     * Método para establecer los campos personalizados del producto.
     */
    public void setCamposPersonalizados(Map<String, Object> camposPersonalizados) {
        this.camposPersonalizados = camposPersonalizados;
    }

    /**
     * Método para agregar un campo personalizado al producto.
     */
    public void agregarCampoPersonalizado(String nombreCampo, Object valorCampo) {
        camposPersonalizados.put(nombreCampo, valorCampo);
    }

    /**
     * Método para modificar un campo personalizado del producto.
     */
    public void modificarCampoPersonalizado(String nombreCampo, Object nuevoValorCampo) {
        if (camposPersonalizados.containsKey(nombreCampo)) {
            camposPersonalizados.put(nombreCampo, nuevoValorCampo);
        } else {
            System.out.println("El campo personalizado '" + nombreCampo + "' no existe.");
        }
    }

    /**
     * Método que devuelve una representación en cadena del objeto Producto.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Producto{");
        sb.append("nombre='").append(nombre).append("', ");
        sb.append("cantidad=").append(cantidad).append(", ");
        sb.append("precio=").append(precio).append(", ");
        sb.append("proveedor='").append(proveedor).append("', ");
        for (Map.Entry<String, Object> entry : camposPersonalizados.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append(", ");
        }
        if (!camposPersonalizados.isEmpty()) {
            sb.delete(sb.length() - 2, sb.length()); // Eliminar la coma y el espacio al final
        }
        sb.append("}");
        return sb.toString();
    }

    /**
     * Método para convertir un documento de MongoDB a un objeto Producto.
     */
    public Producto documentoAProducto(Document doc) {
        ObjectId id = doc.getObjectId("_id");
        String nombre = doc.getString("nombre");
        int cantidad = doc.getInteger("cantidad");
        double precio = doc.getDouble("precio");
        String proveedor = doc.getString("proveedor");
        Producto producto = new Producto(id, nombre, cantidad, precio, proveedor);
        
        // Agregar campos personalizados al producto
        for (Map.Entry<String, Object> entry : doc.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (!key.equals("_id") && !key.equals("nombre") && !key.equals("cantidad") 
                    && !key.equals("precio") && !key.equals("proveedor")) {
                producto.agregarCampoPersonalizado(key, value);
            }
        }
        
        return producto;
    }
}
